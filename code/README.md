# T�tulo: TEMPLATE PARA GENERAR EL README.md GIT (Sintaxis Markdown)

## Subtitulo (2do nivel)

### Subtitulo (3er nivel)

#### Subtitulo (4to nivel)

Texto normal 

*Texto en cursiva*

**Texto en negrita**

~~Texto tachado~~

***Texto en negrita y cursiva***

- Listado 
- Listado 

1. Numeraci�n
2. Numeraci�n

```
$ cd
$ sudo apt-get update
$ sudo apt-get upgrade
$ exit
```
El anterior es un segmento de c�digo de prop�sito general (sin resaltado de sintaxis por lenguaje de programaci�n)

```cpp
int a;
int b;
bool done; 

int c = a+b;

if( c > 10)
{
	done = true;
}
else
{
	done = false:
}
```

El anterior es un segmento de c�digo en c++ con resaltado de sintaxis por lenguaje de programaci�n. Las opciones de resaltado m�s usadas son: c, cpp, python, CMake, MATLAB. Para mayor informaci�n o el listado completo del resaltado de sintaxis, remitirse a:

[Github Syntax Highlighting](https://docs.github.com/es/github/writing-on-github/creating-and-highlighting-code-blocks#syntax-highlighting)

[Documentaci�n Markdown](https://markdown.es/sintaxis-markdown/)

Si los anteriores enlaces no funciona, se puede utilizar los siguientes links <https://docs.github.com/es/github/writing-on-github/creating-and-highlighting-code-blocks#syntax-highlighting> y <https://markdown.es/sintaxis-markdown/>

Los cuatro pasos anteriores fueron hiperv�nculos a una p�gina web

> Esta es una cita

[Ver la imagen](/img/logo-USTA.png)

El anterior es un enlace relativo a un archivo local. Ac� se puede utilizar las reglas de navegaci�n de Linux por terminal, como los comando ./ (Direcci�n del directorio actual) y ../ (Volver al directorio padre/anterior/o superior)

*** 

La anterior fue una regla horizontal (Separaci�n de secci�n de forma visual por medio de una l�nea)

![Logo de la Santo Tom�s (USTA)](/img/logo-USTA.png)

La anterior es la forma como se agregan im�genes bajo la sintaxis markdown

Si se desea hacer uso de los s�mbolos reservados y que sean ignorados por el int�rprete, se utiliza el auxiliar '\' (barra invertida. Ejemplo

\*\*Esto no va a estar en negrita**

Para hacer una revisi�n de c�mo quedar� el texto antes de subirlo a un repositorio git, se hacer uso del siguiente int�rprete online: 

[make a readme](https://www.makeareadme.com/)

*** 

Realizado por 
Hern�n Josu� Hern�ndez Lamprea
hernan.hernandez01@ustabuca.edu.co
Ing. Mecatr�nico, Esp. Gerencia de proyectos
Universidad Santo Tom�s seccional Bucaramanga, Colombia